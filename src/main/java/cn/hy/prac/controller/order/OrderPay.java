package cn.hy.prac.controller.order;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.hy.prac.constant.Constant;

/**
 * 整合接入支付宝测试
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/order")
public class OrderPay {


	@Value("${hy.hyPayUrl}")
	private String hyPayUrl;
	
	/**
	 * 发起支付订单
	 * @param type 支付类型，此时测试只用支付宝
	 * @return
	 */
	@RequestMapping(value="/pay/{type}", method=RequestMethod.GET)
	public String startPay(@PathVariable("type")int type, HttpServletRequest request){
		
		//生成订单号
		String orderNum = Constant.genOrdernum();
		
		System.out.println(hyPayUrl + "/bpay/send/"+orderNum+"/"+type);
		
		return "redirect:"+hyPayUrl + "/bpay/send/"+orderNum+"/"+type;
	}
	
}
