package cn.hy.prac.constant;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Constant {

	//session中存储当前登录的用户key
	public static final String SESSION_USER_KEY = "banner";

	//当前页面最多展现多少条数据
	public static final int PAGE_MAX = 10;
	
	private static Random random = new Random();
	private static SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
	
	/**
	 * 生成订单号
	 * hy + 17位当前日期 + 5位随机数
	 * @return
	 * @author haoyi
	 */
	public static String genOrdernum() {
		return "hy" + format.format(new Date())+(random.nextInt(89999)+10000);
	}
}
