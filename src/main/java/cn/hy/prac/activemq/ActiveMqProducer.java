package cn.hy.prac.activemq;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jms.Queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * MQ生产者
 * @author Administrator
 *
 */
@Component //注册成Bean -- spring自动管理
@EnableScheduling //开启定时任务
public class ActiveMqProducer {

	 @Autowired
     private JmsMessagingTemplate jmsMessagingTemplate;//使用JmsMessagingTemplate将消息放入队列

     @Autowired
     private Queue queue;
 
     //@Scheduled(cron = "0 0/1 * * * ?")//每一分钟执行1次,将消息放入队列内
     public void send() {
        this.jmsMessagingTemplate.convertAndSend(this.queue, "测试消息队列----------"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        System.out.println(this.queue+"测试消息队列----------"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
     }
	
}
