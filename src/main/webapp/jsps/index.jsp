<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>---My First Boot Page---</title>
<link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<table class="table">
	<caption>First Table Page</caption>
	<thead>
		<tr>
			<th>ID</th>
			<th>真实姓名</th>
			<th>登录姓名</th>
			<th>性别</th>
		</tr>
	</thead>
	<tbody>
	<c:forEach items="${bannerList }" varStatus="v" var="banner">
		<tr 
		<c:if test="${v.index%3 == 0 }">class="active"</c:if>
		<c:if test="${v.index%3 == 1 }">class="success"</c:if>
		<c:if test="${v.index%3 == 2 }">class="warning"</c:if>
		>
			<td>${banner.id }</td>
			<td>${banner.trueName }</td>
			<td>${banner.loginName }</td>
			<td>
			<c:if test="${banner.sex eq '0' }">男</c:if>
			<c:if test="${banner.sex eq '1' }">女</c:if>
			</td>
		</tr>
	</c:forEach>
	</tbody>
</table>
<div align="center">
	<a href="${pageContext.request.contextPath }/head/headList" target="black"><font color="red" size="48px">跳转另一个页面~~~~</font></a>
</div>
</body>
</html>