<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>---My First Boot Page---</title>
<link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">  
<script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<table class="table">
	<caption>First Table Page</caption>
	<thead>
		<tr>
			<th>ID</th>
			<th>~~~~</th>
			<th>~~~~</th>
			<th>~~~~</th>
		</tr>
	</thead>
	<tbody>
	<c:forEach items="${headList }" varStatus="v" var="head">
		<tr 
		<c:if test="${v.index%3 == 0 }">class="active"</c:if>
		<c:if test="${v.index%3 == 1 }">class="success"</c:if>
		<c:if test="${v.index%3 == 2 }">class="warning"</c:if>
		>
			<td>${head.id }</td>
			<td>${head.jxmc }</td>
			<td>${head.sfzmhm }</td>
			<td>
			${head.zwtzm }
			</td>
		</tr>
	</c:forEach>
	</tbody>
</table>
</body>
</html>