package cn.hy.prac.service.banner;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.1.3
 * 2017-05-05T11:01:38.113+08:00
 * Generated source version: 3.1.3
 * 
 */
@WebServiceClient(name = "BannerServiceService", 
                  wsdlLocation = "http://localhost:9002/bus/bannerService?wsdl",
                  targetNamespace = "http://banner.service.prac.hy.cn/") 
public class BannerServiceService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://banner.service.prac.hy.cn/", "BannerServiceService");
    public final static QName BannerServicePort = new QName("http://banner.service.prac.hy.cn/", "BannerServicePort");
    static {
        URL url = null;
        try {
            url = new URL("http://localhost:9002/bus/bannerService?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(BannerServiceService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "http://localhost:9002/bus/bannerService?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public BannerServiceService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public BannerServiceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public BannerServiceService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    public BannerServiceService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public BannerServiceService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public BannerServiceService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    




    /**
     *
     * @return
     *     returns BannerService
     */
    @WebEndpoint(name = "BannerServicePort")
    public BannerService getBannerServicePort() {
        return super.getPort(BannerServicePort, BannerService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns BannerService
     */
    @WebEndpoint(name = "BannerServicePort")
    public BannerService getBannerServicePort(WebServiceFeature... features) {
        return super.getPort(BannerServicePort, BannerService.class, features);
    }

}
